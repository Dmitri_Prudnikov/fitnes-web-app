package fitness.data.controllers;

import fitness.data.controllers.handlers.CoachExceptionHandler;
import fitness.data.controllers.handlers.UserExceptionHandler;
import fitness.data.exceptions.CoachNotFoundException;
import fitness.data.services.CoachService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = CoachController.class,
            excludeAutoConfiguration = SecurityAutoConfiguration.class)
@ContextConfiguration(
        classes = {CoachController.class, CoachExceptionHandler.class})
class CoachControllerTest {
    private static final int COMMON_ID = 3;
    private static final String CONTROLLER_MAPPING = "/api/coaches";
    @Autowired
    private MockMvc mvc;

    @MockBean
    private CoachService service;

    @Test
    public void findAllWhenNoneExist() throws Exception {
        when(service.getCoaches()).thenReturn(Collections.emptyList());
        mvc.perform(get("/api/coaches")).andExpect(status().isOk()).andExpect(content().json("[]"));
    }
    
    @Test
    public void findByIdWhenNoneExist() throws Exception {
        when(service.getById(COMMON_ID)).thenThrow(CoachNotFoundException.class);
        mvc.perform(get(CONTROLLER_MAPPING + "/ids/" + COMMON_ID)).andExpect(status().isNotFound())
                .andExpect(content().string(""));
    }
}