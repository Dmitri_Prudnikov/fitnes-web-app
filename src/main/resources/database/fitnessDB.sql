CREATE TABLE "tariff"
(
    "name"  varchar(50) PRIMARY KEY NOT NULL,
    "price" integer
);

CREATE TABLE "coach"
(
    "id"                 serial PRIMARY key not null,
    "name"               varchar(50),
    "main_info"          text,
    "education"          text,
    "work_history"       text,
    "about_me"           text,
    "main_photo"         bytea,
    "second_photo"       bytea,
    "preview_photo"      bytea,
    "preview_main_text"  text,
    "preview_small_text" text
);

CREATE TABLE "tariff_description"
(
    "id"     serial PRIMARY KEY NOT NULL,
    "info"   text,
    "tariff" varchar(50) references "tariff" (name)
);

create table "template_for_nutrition"
(
    "id"                serial PRIMARY KEY NOT NULL,
    "pdf_with_template" bytea,
    "tariff"            varchar(50) references "tariff" (name)
);

CREATE TABLE users
(
    "id"            serial PRIMARY KEY NOT NULL,
    "email"         varchar(70) unique not null,
    "photo"         bytea,
    "tariff"        varchar(50) references "tariff" (name),
    "first_name"    varchar(50),
    "last_name"     varchar(50),
    "password"      varchar(70),
    "roles"         varchar(15),
    initial_weight  varchar(10),
    required_weight varchar(10)
);

CREATE TABLE "workout"
(
    "id"        serial PRIMARY KEY NOT NULL,
    "program_1" varchar(30),
    "program_2" varchar(30),
    "program_3" varchar(30)
);

create table category
(
    "category_name" varchar(50) primary key
);

create table user_video_category
(
    "id"       serial primary key,
    "user_id"  integer references users (id),
    "category" varchar(50) references "category" (category_name)
);



CREATE TABLE "video"
(
    "id"            serial PRIMARY KEY NOT NULL,
    "coach_id"      integer references coach (id),
    "category_name" varchar(50) references category (category_name),
    "video_path"    varchar,
    "upload_date"   date
);


CREATE TABLE "weight"
(
    "id"     serial PRIMARY KEY NOT NULL,
    "weight" varchar(10),
    "date"   date,
    user_id  integer references users (id)
);


CREATE TABLE "specialization"
(
    "id"       serial PRIMARY KEY,
    "info"     varchar(50),
    "coach_id" integer references coach (id)
);
