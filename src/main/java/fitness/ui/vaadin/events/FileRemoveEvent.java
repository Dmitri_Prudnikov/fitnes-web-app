package fitness.ui.vaadin.events;

import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.DomEvent;
import com.vaadin.flow.component.upload.Upload;

@DomEvent("file-remove")
public class FileRemoveEvent extends ComponentEvent<Upload> {
    public FileRemoveEvent(Upload source, boolean fromClient) {
        super(source, fromClient);
    }
}
