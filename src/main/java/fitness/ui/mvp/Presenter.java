package fitness.ui.mvp;

public abstract class Presenter <T extends View> {
    protected T view;

    public void onAttach(T view) {
        this.view = view;
    }

    public void onDetach() {
        this.view = null;
    }
}
