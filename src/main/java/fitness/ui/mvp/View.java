package fitness.ui.mvp;

import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;

public interface View {
    default void showNotification(String message, NotificationVariant notificationVariant) {
        Notification notification = Notification.show(
                message,
                5000,
                Notification.Position.BOTTOM_END
        );
        notification.addThemeVariants(notificationVariant);
    }

    void init();

    void bind();
}
