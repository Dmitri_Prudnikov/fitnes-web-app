package fitness.ui.dialogs.workout;

import fitness.ui.mvp.View;

public interface CreateWorkoutDialog extends View {
    void open();

    void close();

}
