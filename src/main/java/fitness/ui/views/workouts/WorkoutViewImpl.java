package fitness.ui.views.workouts;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.StyleSheet;
import com.vaadin.flow.component.html.*;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import fitness.data.entity.Tariff;
import fitness.data.entity.TariffDescription;
import fitness.ui.dialogs.workout.CreateWorkoutDialog;
import fitness.ui.dialogs.workout.CreateWorkoutDialogImpl;
import fitness.ui.dialogs.workout.CreateWorkoutPresenter;
import fitness.ui.views.utils.CustomFooter;
import fitness.ui.views.utils.CustomHeader;
import fitness.ui.views.utils.DropDownMenuColor;
import java.util.Collections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

@Transactional
@Route("workouts")
@StyleSheet("./fonts.css")
@StyleSheet("./workouts-styles/workout.css")
public class WorkoutViewImpl extends VerticalLayout implements WorkoutView {

    private final WorkoutPresenter presenter;
    private final CreateWorkoutPresenter workoutPresenter;
    private final HashMap<String, List<String>> workouts;
    private final Image dumbbellsImage = new Image("/images/workout/dumbbells.png", "a/photo pic");

    public WorkoutViewImpl(@Autowired WorkoutPresenter presenter,
        @Autowired CreateWorkoutPresenter workoutPresenter) {
        this.presenter = presenter;
        this.workoutPresenter = workoutPresenter;
        this.workouts = new HashMap<>();
    }

    @Override
    @PostConstruct
    public void init() {
        setWidth("100%");
        setClassName("no-distance");

        Div firstFrame = getFirstFrame();
        VerticalLayout secondFrame = getSecondFrame();
        VerticalLayout thirdFrame = getThirdFrame();
        Component[] components = {new CustomHeader(DropDownMenuColor.GREEN), firstFrame, secondFrame, thirdFrame,
            new CustomFooter()};
        add(components);
    }


    private VerticalLayout getThirdFrame() {
        VerticalLayout page = new VerticalLayout();
        page.setClassName("no-distance");

        H2 title = new H2("ГОТОВЫ К ФИГУРЕ СВОЕЙ МЕЧТЫ?");
        title.setClassName("dream-figure");

        H3 content = new H3("Выбери абонемент и стань частью нашего проекта!");
        content.setClassName("content-be-part");

        Div frame = new Div();
        frame.setWidth("100%");
        frame.setClassName("frame-tariff");
        frame.setHeightFull();

        String lite = "LITE - идеальная эффективная программа для домашних условий!";
        String base = "BASE - тариф для тех, кому, помимо тренировок, необходима помощь в построении питания.";
        String maximum = "MAXIMUM - контроль + мотивация + корректировки и консультации = максимальный эффект!";
        VerticalLayout tariffLite = setupTariff(lite, "Lite");
        VerticalLayout tariffBase = setupTariff(base, "Base");
        VerticalLayout tariffMaximum = setupTariff(maximum, "Maximum");

        Component[] components = {tariffLite, tariffBase, tariffMaximum};
        frame.add(components);
        page.add(title, content, frame);
        return page;
    }

    private VerticalLayout setupTariff(String content, String currentTariff) {
        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.setClassName("full-tariff");
        H4 description = new H4(content);
        description.setClassName("tariff");
        VerticalLayout rectangle = getTariffRectangle(currentTariff);
        verticalLayout.add(description, rectangle);
        return verticalLayout;
    }

    private VerticalLayout getTariffRectangle(String tariffName) {
        H5 text = new H5(tariffName);
        text.setClassName("text-gradient");

        Div gradient = new Div();
        gradient.setClassName(tariffName.toLowerCase(Locale.ROOT) + "-gradient");
        gradient.add(text);
        Tariff tariff = presenter.getTariff(tariffName);
        UnorderedList list = new UnorderedList();
        for (TariffDescription s : presenter.getDescriptionsByTariffName(tariffName)) {
            ListItem item = new ListItem(s.getInfo());
            item.getStyle().set("margin-bottom", "10px");
            item.setWidth("80%");
            item.setClassName("marker");
            list.add(item);
        }
        list.getStyle().set("margin-bottom", "0px");

        Div line = new Div();
        line.setClassName("line");

        Div cost = new Div();
        H5 costText = new H5(tariff.getPrice() + " руб.");
        costText.setClassName("cost");
        H6 month = new H6("в месяц");
        month.setClassName("month");
        cost.add(costText, month);
        cost.getStyle().set("padding-right", "38%").set("padding-bottom", "3%");

        VerticalLayout rectangle = new VerticalLayout();
        rectangle.setClassName("rectangle");
        rectangle.add(gradient, list, line, cost, setupButton(tariff));
        return rectangle;
    }

    private Button setupButton(Tariff tariff) {
        Button buy = new Button("Купить",
            buttonClickEvent -> {
                workoutPresenter.setTariff(tariff);
                workoutPresenter.setWorkouts(workouts);
                CreateWorkoutDialog createWorkoutDialog = new CreateWorkoutDialogImpl(workoutPresenter);
                createWorkoutDialog.open();
            });
        buy.setClassName("buy");
        return buy;
    }

    private VerticalLayout getSecondFrame() {
        VerticalLayout page = new VerticalLayout();
        page.setClassName("no-distance");

        H2 title = new H2("Наши программы");
        title.setClassName("programs");
        page.add(title);

        Div frame = new Div();
        frame.setClassName("background");
        frame.setWidth("100%");

        Div workoutFrame = new Div();

        workoutFrame.setClassName("white-background");
        workoutFrame.setWidth("90%");

        Div row1 = new Div();
        row1.setClassName("center");
        row1.getClassNames().add("flex");
        row1.setWidth("100%");

        for (int i = 1; i < 7; i++) {
            row1.add(setRows(i));
        }

        workoutFrame.add(row1);
        frame.add(workoutFrame);
        page.add(frame);
        return page;
    }

    private HorizontalLayout setRows(int i) {
        HorizontalLayout block = new HorizontalLayout();

        block.setClassName("center-align");
        block.getClassNames().add("block");
        Paragraph number = new Paragraph(String.valueOf(i));
        number.setClassName("numbers");

        Image figure = new Image("images/workout/figures/" + i + ".png", "a/photo pic");
        figure.setHeight("78px");
        figure.setWidth("78px");
        figure.getStyle().set("margin-left", "auto");
        VerticalLayout text = new VerticalLayout(figure);

        Div ul = new Div();
        List<String> descriptions = presenter.getWorkouts(i);
        workouts.put("Программа " + i, descriptions);

        for (String s : descriptions) {
            Paragraph li = new Paragraph(s);
            ul.add(li);
        }
        text.add(ul);
        text.setWidth("100%");
        block.add(number, text);
        return block;
    }

    private Div getFirstFrame() {
        Div frame = new Div();
        frame.getClassNames().add("flex");
        frame.getClassNames().add("space-around");
        frame.getClassNames().add("frame-trains");

        H2 title = new H2("ВЫПОЛНЯЙ ТЕ КОМПЛЕКСЫ, КОТОРЫЕ ПО ДУШЕ!");
        title.setClassName("titleOne");

        Paragraph paragraph = new Paragraph("Каждый день имеет сбалансированное распределение тренировочных программ");
        paragraph.setClassName("paragraph");

        VerticalLayout textBlock = new VerticalLayout();
        textBlock.setWidth("55%");
        textBlock.setMinWidth("400px");
        textBlock.getClassNames().add("text-block");
        textBlock.getClassNames().add("center");
        textBlock.add(title, paragraph);
        dumbbellsImage.setClassName("photo");

        frame.add(textBlock, dumbbellsImage);
        return frame;
    }

    @Override
    public void bind() {
    }
}
