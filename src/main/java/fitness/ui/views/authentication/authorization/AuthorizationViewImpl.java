package fitness.ui.views.authentication.authorization;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.StyleSheet;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.EmailField;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.VaadinService;
import fitness.data.entity.User;
import fitness.data.exceptions.InvalidCredentialsException;
import fitness.ui.views.authentication.AuthenticationViewImpl;
import fitness.ui.views.utils.Validator;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import javax.servlet.http.Cookie;

@Route("authorization")
@StyleSheet("./fonts.css")
@StyleSheet("./authentication/authentication.css")
public class AuthorizationViewImpl extends AuthenticationViewImpl {

    private final AuthorizationPresenter presenter;

    private final VerticalLayout form = new VerticalLayout();
    private PasswordField password;
    private EmailField email;
    private final Binder<User> binder;

    public AuthorizationViewImpl(@Autowired AuthorizationPresenter presenter) {
        this.binder = new Binder<>();
        this.presenter = presenter;
    }

    @PostConstruct
    @Override
    public void init() {
        setupForm();
        bind();
        super.getFormBackground().add(form);
        super.init();
    }

    private void setupForm() {
        email = new EmailField("Email", "name@domain.com");
        email.setErrorMessage("Пожалуйста введите email");
        email.setWidth(390, Unit.PIXELS);
        email.setClassName("text");

        password = new PasswordField("Пароль", "Введите пароль");
        password.setWidth(390, Unit.PIXELS);
        password.setClassName("text");

        H2 title = new H2("Вход");
        title.setClassName("title");

        Button loginButton = new Button("Войти", event -> {
            try {
                Cookie jwtCookie = presenter.authorize(email.getValue(), password.getValue());
                VaadinService.getCurrentResponse().addCookie(jwtCookie);
                // cant use .navigate("profile") because it redirects without getting response
                // and therefore without obtaining jwtCookie
                // but profile needs this cookie to identify user
                UI.getCurrent().getPage().setLocation("profile");
            } catch (InvalidCredentialsException e) {
                showNotification("Неправильный пароль или почта", NotificationVariant.LUMO_ERROR);
            }
        });

        loginButton.setClassName("registration-button");

        Button registrationButton =
            new Button("Зарегистрироваться", event -> UI.getCurrent().navigate("registration"));
        registrationButton.setClassName("registrate-button");

        Button forgetPassword =
            new Button("Забыли пароль?", event -> UI.getCurrent().navigate("forget-password"));
        forgetPassword.setClassName("forget-password-button ");

        HorizontalLayout buttons = new HorizontalLayout();
        buttons.add(forgetPassword, registrationButton);
        buttons.setClassName("buttons-authentication");

        form.add(title, email, password, loginButton, buttons);
        form.addClassName("registration-form");
        form.addClassName("form");
    }

    @Override
    public void bind() {
        binder.forField(password)
            .withValidator(Validator.getEmptyStringValidator(), "Пароль не может быть пустым")
            .withValidator(Validator.getPasswordValidator(),
                "Пароль должен содержать не менее 8 символов");

        binder.forField(email).withValidator(Validator.getEmptyStringValidator(),
            "Email не может быть пустым");
    }

    @Override
    public void clearForms(User user) {
        binder.readBean(user);
    }
}
