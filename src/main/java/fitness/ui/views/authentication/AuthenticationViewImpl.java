package fitness.ui.views.authentication;

import com.vaadin.flow.component.dependency.StyleSheet;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import fitness.data.entity.User;
import lombok.Getter;

import javax.annotation.PostConstruct;

@StyleSheet("./fonts.css")
@StyleSheet("./authentication/authentication.css")
public abstract class AuthenticationViewImpl extends Div implements AuthenticationView {
    @Getter
    private final HorizontalLayout formBackground = new HorizontalLayout();
    private final Image image = new Image("/images/a.jpeg", "a/reg pic");

    @PostConstruct
    @Override
    public void init() {
        setClassName("background");
        image.setClassName("image");
        formBackground.getClassNames().add("form-background");
        formBackground.add(image);
        add(formBackground);
    }

    public abstract void clearForms(User user);
}
