package fitness.ui.views.admin.videoupload;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.upload.Upload;
import com.vaadin.flow.router.Route;
import fitness.data.entity.Video;
import fitness.ui.vaadin.components.CustomUploadI18N;
import fitness.ui.vaadin.components.CustomVideoReceiver;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;


@Route("video-upload")
@CssImport("./videouploader-styles/video-uploader.css")
public class VideoUploadViewImpl extends VerticalLayout implements VideoUploadView {

  private final VideoUploadPresenter videoUploadPresenter;

  private static final String EXTENSION = ".mp4";

  public VideoUploadViewImpl(@Autowired VideoUploadPresenter videoUploadPresenter) {
    this.videoUploadPresenter = videoUploadPresenter;
    this.videoUploadPresenter.onAttach(this);
  }

  @PostConstruct
  @Override
  public void init() {
    addClassName("video-upload-div");
    CustomVideoReceiver fileBuffer = new CustomVideoReceiver(videoUploadPresenter);
    TextField workoutNameField = createWorkoutNameTextField();
    Button videoUploadButton = new Button("Загрузить");
    videoUploadButton.addClassName("upload-button");
    Upload fileUpload = new Upload(fileBuffer);

    setupVideoUpload(fileUpload, workoutNameField, videoUploadButton, fileBuffer);

    add(fileUpload, workoutNameField, videoUploadButton);
  }

  @Override
  public void bind() {
  }

  private TextField createWorkoutNameTextField() {
    TextField workoutName = new TextField();
    workoutName.setClearButtonVisible(true);
    workoutName.setRequired(true);
    workoutName.setPlaceholder("Введите название тренировки");
    return workoutName;
  }

  private void setupVideoUpload(Upload fileUpload, TextField workoutNameField,
                                Button videoUploadButton, CustomVideoReceiver fileBuffer) {
    fileUpload.setAcceptedFileTypes(EXTENSION);
    fileUpload.setAutoUpload(false);
    fileUpload.setI18n(new CustomUploadI18N());

    fileUpload.addFileRejectedListener(event -> {
      String errorMessage = event.getErrorMessage();
      Notification notification = Notification.show(
              errorMessage,
              5000,
              Notification.Position.MIDDLE
      );
      notification.addThemeVariants(NotificationVariant.LUMO_ERROR);
    });

    fileUpload.addStartedListener(event -> fileBuffer.setWorkoutName(workoutNameField.getValue()));

    videoUploadButton.addClickListener(e -> {
      if (!workoutNameField.getValue().isEmpty()) {
        fileUpload.getElement().callJsFunction("uploadFiles");
      } else {
        Notification notification = Notification.show(
                "Заполните поле с названием тренировки",
                5000,
                Notification.Position.MIDDLE
        );
        notification.addThemeVariants(NotificationVariant.LUMO_ERROR);
      }
    });

    Label videoUploadLabel = new Label("Загрузите видео-тренировку");
    videoUploadLabel.setClassName("video-upload-label");
    fileUpload.setDropLabel(videoUploadLabel);
  }
}
