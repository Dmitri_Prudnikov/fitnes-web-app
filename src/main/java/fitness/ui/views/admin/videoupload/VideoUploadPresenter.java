package fitness.ui.views.admin.videoupload;

import com.vaadin.flow.spring.annotation.SpringComponent;
import fitness.data.entity.Category;
import fitness.data.entity.Video;
import fitness.data.services.CategoryService;
import fitness.data.services.VideoService;
import fitness.ui.mvp.Presenter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;

@SpringComponent
@Scope(value = BeanDefinition.SCOPE_PROTOTYPE)
public class VideoUploadPresenter extends Presenter<VideoUploadView> {

    private final VideoService videoService;
    private final CategoryService categoryServiceService;

    public VideoUploadPresenter(@Autowired VideoService videoService,
        @Autowired CategoryService categoryServiceService) {
        this.videoService = videoService;
        this.categoryServiceService = categoryServiceService;
    }

    public void saveVideo(Video video) {
        videoService.addVideo(video);
    }

    public void saveVideo(String workoutName, String videoPath) {
        Video video = new Video();
        video.setVideoPath(videoPath);
        Category category = categoryServiceService.getCategoryByName(workoutName);
        category.getVideos().add(video);
        video.setCategory(category);

        saveVideo(video);
        categoryServiceService.saveCategory(category);
    }
}
