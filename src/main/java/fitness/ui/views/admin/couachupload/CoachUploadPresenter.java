package fitness.ui.views.admin.couachupload;

import fitness.data.entity.Coach;
import fitness.data.entity.Specialization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import com.vaadin.flow.spring.annotation.SpringComponent;
import fitness.data.services.CoachService;
import fitness.data.services.SpecializationService;
import fitness.ui.mvp.Presenter;
import lombok.Getter;

@SpringComponent
@Scope(value = BeanDefinition.SCOPE_PROTOTYPE)
public class CoachUploadPresenter extends Presenter<CoachUploadView> {

  private final CoachService coachService;
  private final SpecializationService specializationService;

  @Getter
  private Coach coachEntity = new Coach();

  @Autowired
  public CoachUploadPresenter(CoachService coachService,
      SpecializationService specializationService) {
    this.coachService = coachService;
    this.specializationService = specializationService;
  }

  public Coach update() {
    Coach result = coachService.update(coachEntity);
    for (Specialization specialization : coachEntity.getSpecializations()) {
      System.out.println(specialization.getInfo());
    }
    coachEntity.getSpecializations().forEach(specializationService::update);
    coachService.updateCoachesParallel();
    return result;
  }

}
