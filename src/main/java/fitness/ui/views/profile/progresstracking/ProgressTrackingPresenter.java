package fitness.ui.views.profile.progresstracking;

import com.vaadin.flow.spring.annotation.SpringComponent;
import fitness.data.entity.Weight;
import fitness.data.services.WeightService;
import fitness.ui.mvp.Presenter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;

import java.time.ZoneId;
import java.util.Date;
import java.util.List;

@SpringComponent
@Scope(value = BeanDefinition.SCOPE_PROTOTYPE)
public class ProgressTrackingPresenter extends Presenter<ProgressTrackingView>
{
    private final WeightService service;

    private final Integer weightsCount = 6;
    
    public ProgressTrackingPresenter(@Autowired WeightService service)
    {
        this.service = service;
    }

    public List<Weight> getCurrentWeights()
    {
        return service.getWeightByUserId(9);
    }

    public Integer getWeightsCount()
    {
        return weightsCount;
    }
    
    public String getWeights()
    {
        return String.join(",", getCurrentWeights()
                .subList(Math.max(getCurrentWeights().size() - weightsCount, 0), getCurrentWeights().size())
                .stream().map(Weight::getWeight).map(String::valueOf).toList());
    }

    public String getDates()
    {
        return String.join(",", getCurrentWeights()
                .subList(Math.max(getCurrentWeights().size() - weightsCount, 0), getCurrentWeights().size())
                .stream().map(Weight::getDate).map(this::convertDate).toList());
    }

    private String convertDate(Date date)
    {
        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate().getDayOfMonth() + "." +
                date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate().getMonthValue();
    }

    public Integer getInitialWeight()
    {
        //TODO get from db
        return 0;
    }

    public Integer getRequiredWeight()
    {
        //TODO get from db
        return 0;
    }

    public String getChanges()
    {
        return "0";
        /*return String.valueOf(getCurrentWeights().size() > 1
                ? getCurrentWeights().get(getCurrentWeights().size() - 1).getWeight()
                - getCurrentWeights().get(getCurrentWeights().size() - 2).getWeight()
                : 0);*/
    }

    public String getLeftWeight()
    {
        return "0";
        /*return String.valueOf(getCurrentWeights().size() > 0 && hasRequiredWeightSpecified()
                ? getRequiredWeight() - getCurrentWeights().get(getCurrentWeights().size() - 1).getWeight()
                : 0);*/
    }

    public String getCurrentWeight()
    {
        return "0";
        /*return String.valueOf(getCurrentWeights().size() > 0
                ? getCurrentWeights().get(getCurrentWeights().size() - 1) : 0);*/
    }

    private boolean hasRequiredWeightSpecified()
    {
        //TODO get from db
        return true;
    }

    private boolean hasInitialWeightSpecified()
    {
        //TODO get from db
        return true;
    }
}
