package fitness.ui.views.coaches.coachepage;

import fitness.data.entity.Coach;
import fitness.data.entity.Specialization;
import fitness.data.services.CoachService;
import fitness.data.services.SpecializationService;
import fitness.ui.mvp.Presenter;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value = BeanDefinition.SCOPE_PROTOTYPE)
public class CoachPagePresenter extends Presenter<CoachPageView> {

    private final CoachService coachService;
    private final SpecializationService specializationService;

    public CoachPagePresenter(
        @Autowired CoachService coachService,
        @Autowired SpecializationService specializationService
    ) {
        this.coachService = coachService;
        this.specializationService = specializationService;
    }
    public void addSpecializations(List<Specialization> list){
        for (Specialization specialization : list) {
            specializationService.update(specialization);
        }
    }
    public Coach updateCoach(Coach coach) {
        return coachService.update(coach);
    }

    public Coach getCoachById(int id) {
        return coachService.getById(id);
    }
}