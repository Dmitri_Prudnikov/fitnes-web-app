package fitness.ui.views.landing;

import com.vaadin.flow.spring.annotation.SpringComponent;
import fitness.ui.mvp.Presenter;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;

@SpringComponent
@Scope(value = BeanDefinition.SCOPE_PROTOTYPE)
public class LandingPresenter extends Presenter<LandingView> {
}
