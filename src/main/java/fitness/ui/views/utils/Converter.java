package fitness.ui.views.utils;

import com.vaadin.flow.function.SerializableFunction;
import com.vaadin.flow.function.ValueProvider;

public class Converter {

  public static <SOURCE_TYPE> SerializableFunction<String, String> getConverterInvokeGetterIfNullOrEmpty(
      ValueProvider<SOURCE_TYPE, String> getter, SOURCE_TYPE source) {
    return enteredValue -> {
      if ((enteredValue == null) || enteredValue.isEmpty()) {
        return getter.apply(source);
      }
      return enteredValue;
    };
  }
}
