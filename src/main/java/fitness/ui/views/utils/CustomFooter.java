package fitness.ui.views.utils;

import com.vaadin.flow.component.Html;

public class CustomFooter extends Html {

    public CustomFooter() {
        super(NoThrowFileInputStream.construct(
            "src/main/resources/META-INF/resources/frontend/footer-styles/footer.html"));
    }

}
