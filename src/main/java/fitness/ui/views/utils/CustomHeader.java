package fitness.ui.views.utils;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.contextmenu.MenuItem;
import com.vaadin.flow.component.contextmenu.SubMenu;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Header;
import com.vaadin.flow.component.html.Hr;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.html.Section;
import com.vaadin.flow.component.menubar.MenuBar;
import com.vaadin.flow.component.menubar.MenuBarVariant;

@CssImport("./header-styles/header.css")
public class CustomHeader extends Header {

    private MenuBar menuBar;

    public CustomHeader(DropDownMenuColor color) {
        Div rowOfDivs = new Div();
        rowOfDivs.setClassName("custom-nav-bar");
        setClassName("full-header");
        rowOfDivs.add(setupDomainLabel());
        Section s = new Section();
        s.add(setupMainLabel());
        s.add(setupCoaches());
        s.add(setupWorkouts());
        s.getClassNames().add("nav-panel");
        rowOfDivs.add(s);
        rowOfDivs.add(setupDropDownMenu());
        add(rowOfDivs);
        menuBar.getStyle().set("background", color.getColorRepresentation());
    }

    private Image setupDomainLabel() {

        Image image = new Image("/images/header/logo.webp", "logo");
        image.getClassNames().add("logo");

        return image;
    }

    private Div setupMainLabel() {
        Div div = new Div();
        Text t = new Text("Главная");
        div.add(t);
        div.addClickListener(e -> UI.getCurrent().navigate("landing"));
        return div;
    }

    private Div setupCoaches() {
        Div div = new Div();
        Text t = new Text("Тренеры");
        div.add(t);
        div.addClickListener(e -> UI.getCurrent().navigate("coaches"));
        return div;
    }

    private Div setupWorkouts() {
        Div div = new Div();
        Text t = new Text("Тренировки");
        div.add(t);
        div.addClickListener(e -> UI.getCurrent().navigate("workouts"));
        return div;
    }

    private Component setupDropDownMenu() {
        MenuBar menuBar = new MenuBar();
        menuBar.addThemeVariants(MenuBarVariant.LUMO_TERTIARY);
        menuBar.setClassName("drop-down-menu-bar");
        MenuItem item = menuBar.addItem("Личный кабинет");
        item.getElement().getStyle().set("color", "black");
        item.getElement().getStyle().set("padding", "30px");

        SubMenu subMenu = item.getSubMenu();

        Button mainButton = new Button("Персональная информация",
            e -> UI.getCurrent().navigate("personal-information")); //not yet implemented
        mainButton.setClassName("menu-main-button");
        mainButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
        mainButton.getStyle().set("color", "black");

        Button progressTrackingButton = new Button("Отслеживание прогресса",
            e -> UI.getCurrent().navigate("progress-tracking"));
        progressTrackingButton.setClassName("menu-progress-tracking-button");
        progressTrackingButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
        progressTrackingButton.getStyle().set("color", "black");

        Button myWorkoutsButton = new Button("Мои тренировки",
            e -> UI.getCurrent().navigate("videos"));
        myWorkoutsButton.setClassName("menu-my-workouts-button");
        myWorkoutsButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
        myWorkoutsButton.getStyle().set("color", "black");

        subMenu.addItem(mainButton);
        subMenu.add(new Hr());
        subMenu.addItem(progressTrackingButton);
        subMenu.add(new Hr());
        subMenu.addItem(myWorkoutsButton);
        this.menuBar = menuBar;
        return menuBar;
    }
}
