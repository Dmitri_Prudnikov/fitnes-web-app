package fitness.data.repositories;

import fitness.data.entity.User;
import fitness.data.entity.Video;
import javax.persistence.criteria.CriteriaBuilder.In;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Repository;

@Repository
public interface VideoRepository extends JpaRepository<Video, Integer> {

    @Query("select v from Video v where v.category.name = ?1")
    List<Video> findByWorkoutName(String workoutName);

    @Query("select v from Video v where v.category.name = ?1 and v.id = ?2")
    Optional<Video> findByWorkoutNameAndWorkoutId(String workoutName, Integer id);

    @Query("select v from Video v where v.category.users = ?1")
    List<Video> findVideosOfUser(User user);

}
