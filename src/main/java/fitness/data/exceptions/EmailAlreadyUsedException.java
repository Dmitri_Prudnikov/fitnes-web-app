package fitness.data.exceptions;

public final class EmailAlreadyUsedException extends RuntimeException {

  public EmailAlreadyUsedException() {
    super();
  }

  public EmailAlreadyUsedException(String message) {
    super(message);
  }

  public EmailAlreadyUsedException(Throwable cause) {
    super(cause);
  }

  public EmailAlreadyUsedException(String message, Throwable cause) {
    super(message, cause);
  }

}
