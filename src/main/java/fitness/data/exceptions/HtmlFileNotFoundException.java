package fitness.data.exceptions;

public class HtmlFileNotFoundException extends RuntimeException {

    public HtmlFileNotFoundException() {
    }

    public HtmlFileNotFoundException(String message) {
        super(message);
    }

    public HtmlFileNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public HtmlFileNotFoundException(Throwable cause) {
        super(cause);
    }
}
