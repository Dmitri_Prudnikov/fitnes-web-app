package fitness.data.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

@Entity
@Getter
@Setter
@Table(name = "users")
@JsonInclude(Include.NON_NULL)
public class User implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tariff")
    private Tariff tariff;

    @Column(name = "email", nullable = false, length = 70)
    private String email;

    @Column(name = "photo")
    private byte[] photo;

    @Column(name = "first_name", length = 50)
    private String firstName;

    @Column(name = "last_name", length = 50)
    private String lastName;

    @Column(name = "password", length = 70)
    private String password;

    @Column(name = "roles", length = 15)
    private String roles;

    @ManyToMany(cascade = {CascadeType.ALL})
    @JoinTable(
        name = "user_video_category",
        joinColumns = {@JoinColumn(name = "user_id")},
        inverseJoinColumns = {@JoinColumn(name = "category")}
    )
    private List<Category> categories = new LinkedList<>();

    @OneToMany(mappedBy = "user")
    private List<Weight> weights = new LinkedList<>();

    @Column(name = "initial_weight", length = 10)
    private String initialWeight;

    @Column(name = "required_weight", length = 10)
    private String requiredWeight;

    public String getFio() {
        if ((lastName == null) || (firstName == null)) {
            return null;
        }
        return lastName + " " + firstName;
    }


    /**
     * Set first name and last name from fio
     *
     * @param fio contains last name and first name in exactly that order with space as delimiter
     * @throws IllegalArgumentException if space isn't presented at all or first space is found at the end of fio
     */
    public void setFio(String fio) {
        try {
            lastName = fio.substring(0, fio.indexOf(' '));
            firstName = fio.substring(fio.indexOf(' ') + 1);
        } catch (IndexOutOfBoundsException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    @JsonIgnore
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return List.of(new SimpleGrantedAuthority(roles));
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    @JsonIgnore
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    @JsonIgnore
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    @JsonIgnore
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    @JsonIgnore
    public boolean isEnabled() {
        return true;
    }

}
