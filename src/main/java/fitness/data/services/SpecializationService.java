package fitness.data.services;

import fitness.data.entity.Specialization;
import java.util.List;
import java.util.Optional;

import javax.persistence.criteria.CriteriaBuilder.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.vaadin.artur.helpers.CrudService;
import fitness.data.repositories.SpecializationRepository;

@Service
public class SpecializationService extends CrudService<Specialization, Integer> {

    private final SpecializationRepository repository;

    public SpecializationService(@Autowired SpecializationRepository repository) {
        this.repository = repository;
    }

    @Override
    public Specialization update(Specialization entity) {
        return super.update(entity);
    }

    @Override
    public Optional<Specialization> get(Integer id) {
        return super.get(id);
    }

    public List<Specialization> findAll() {
        return repository.findAll();
    }

    @Override
    protected JpaRepository<Specialization, Integer> getRepository() {
        return repository;
    }
}
