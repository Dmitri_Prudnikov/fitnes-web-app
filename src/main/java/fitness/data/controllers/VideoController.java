package fitness.data.controllers;

import fitness.data.exceptions.VideoNotFoundException;
import fitness.data.services.VideoService;
import javax.persistence.criteria.CriteriaBuilder.In;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@RestController
@RequestMapping("/api/videos")
public class VideoController
{
  private final VideoService videoService;
  private final ServletContext servletContext;

  private static final String CLASSPATH_TO_VIDEOS = "/META-INF/resources/videos/";
  private static final String MEDIATYPE = "video/mp4";

  public VideoController(@Autowired VideoService videoService,
                         @Autowired ServletContext servletContext)
  {
    this.videoService = videoService;
    this.servletContext = servletContext;
  }

  @GetMapping("ids/{id}")
  public ResponseEntity<byte[]> findById(HttpServletRequest request,
                                         @PathVariable("id") Integer id)
  {
    try
    {
      HttpHeaders headers = new HttpHeaders();
      headers.setContentType(MediaType.parseMediaType(MEDIATYPE));
      String dataDirectory = request.getServletContext().getRealPath(CLASSPATH_TO_VIDEOS);
      String fileName = videoService.findById(id).getVideoPath();
      Path file = Paths.get(dataDirectory, fileName);
      byte[] bytes = Files.readAllBytes(file);
      headers.setCacheControl(CacheControl.noCache().getHeaderValue());
      return new ResponseEntity<>(bytes, headers, HttpStatus.OK);
    }
    catch (VideoNotFoundException e)
    {
      e.printStackTrace();
      return ResponseEntity.notFound().build();
    }
    catch (IOException e)
    {
      e.printStackTrace();
      return ResponseEntity.internalServerError().build();
    }
  }

  @GetMapping("workouts/{workout}/{id}")
  public ResponseEntity<byte[]> findByWorkoutName(@PathVariable("workout") String workoutName,
                                                 @PathVariable("id") Integer id)
  {
    try
    {
      HttpHeaders headers = new HttpHeaders();
      InputStream in = servletContext.getResourceAsStream(CLASSPATH_TO_VIDEOS +
              videoService.findByWorkoutNameAndWorkoutId(workoutName, id));
      byte[] video = IOUtils.toByteArray(in);
      headers.setCacheControl(CacheControl.noCache().getHeaderValue());
      return new ResponseEntity<>(video, headers, HttpStatus.OK);
    }
    catch (VideoNotFoundException e)
    {
      e.printStackTrace();
      return ResponseEntity.notFound().build();
    }
    catch (IOException e)
    {
      e.printStackTrace();
      return ResponseEntity.internalServerError().build();
    }
  }

}
