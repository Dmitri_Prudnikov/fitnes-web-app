package fitness.data.controllers.handlers;

import fitness.data.exceptions.VideoNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class VideoExceptionHandler extends ResponseEntityExceptionHandler
{
  @ExceptionHandler(VideoNotFoundException.class)
  public ResponseEntity<Object> handleVideoNotFound(VideoNotFoundException e)
  {
    return ResponseEntity.notFound().build();
  }
}
