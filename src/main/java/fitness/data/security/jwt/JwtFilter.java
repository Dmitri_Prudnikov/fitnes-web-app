package fitness.data.security.jwt;

import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.filter.OncePerRequestFilter;
import com.vaadin.flow.spring.annotation.SpringComponent;
import fitness.data.services.JwtUserServiceAdapter;

// @SpringComponent
public class JwtFilter extends OncePerRequestFilter {

  private final JwtUserServiceAdapter jwtUserService;
  private final JwtUtils jwtUtils;

  @Autowired
  public JwtFilter(JwtUserServiceAdapter jwtUserService, JwtUtils jwtUtil) {
    this.jwtUserService = jwtUserService;
    this.jwtUtils = jwtUtil;
  }

  @Override
  protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
      FilterChain filterChain) throws ServletException, IOException {
    String jwt = jwtUtils.getJwtFromCookies(request.getCookies());
    if (jwt == null) {
      filterChain.doFilter(request, response);
      return;
    }
    Authentication auth = getAuthentification(jwtUtils.getSubjectFromJwt(jwt));
    SecurityContextHolder.getContext().setAuthentication(auth);
    filterChain.doFilter(request, response);
  }

  private Authentication getAuthentification(String email) {
    UserDetails user = jwtUserService.loadUserByEmail(email);
    return new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities());
  }


}
