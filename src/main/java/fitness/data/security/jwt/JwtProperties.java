package fitness.data.security.jwt;

import javax.crypto.SecretKey;
import org.springframework.boot.context.properties.ConfigurationProperties;
import com.vaadin.flow.spring.annotation.SpringComponent;
import io.jsonwebtoken.security.Keys;

@SpringComponent
@ConfigurationProperties(prefix = "application.jwt")
public class JwtProperties {

  private static final String JWT_COOKIE_NAME = "jwt";
  private int jwtExpirationInSeconds;
  private SecretKey secretKey;
  private String jwtSecretKey;
  private String issuer;

  public String getIssuer() {
    return issuer;
  }

  public void setIssuer(String issuer) {
    this.issuer = issuer;
  }

  public String getJwtSecretKey() {
    return jwtSecretKey;
  }

  public void setJwtSecretKey(String jwtSecretKey) {
    this.jwtSecretKey = jwtSecretKey;
    secretKey = Keys.hmacShaKeyFor(jwtSecretKey.getBytes());
  }

  public void setJwtExpirationInSeconds(int jwtExpirationInSeconds) {
    this.jwtExpirationInSeconds = jwtExpirationInSeconds;
  }

  public int getJwtExpirationInSeconds() {
    return jwtExpirationInSeconds;
  }

  public SecretKey getSecretKey() {
    return secretKey;
  }

  public String getJwtCookieName() {
    return JWT_COOKIE_NAME;
  }
}
