package fitness.data.security.jwt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import fitness.data.exceptions.JwtNotFoundException;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import javax.servlet.http.Cookie;

@Component
public class JwtUtils {

  private final JwtProperties jwtProperties;

  @Autowired
  public JwtUtils(JwtProperties jwtProperties) {
    this.jwtProperties = jwtProperties;
  }

  public String generateToken(String email) {
    return Jwts.builder().setSubject(email).signWith(jwtProperties.getSecretKey()).compact();
  }

  public Cookie generateCookieWithToken(String email) {
    Cookie jwtCookie = new Cookie(jwtProperties.getJwtCookieName(), generateToken(email));
    jwtCookie.setMaxAge(jwtProperties.getJwtExpirationInSeconds());
    jwtCookie.setPath("/");
    return jwtCookie;
  }

  public String getJwtFromCookies(Cookie[] cookies) {
    if (cookies != null) {
      for (Cookie cookie : cookies) {
        if (cookie.getName().equals(jwtProperties.getJwtCookieName())) {
          return cookie.getValue();
        }
      }
    }
    throw new JwtNotFoundException();
  }

  public String getSubjectFromJwt(String jwt) {
    Jws<Claims> jws = Jwts.parserBuilder().setSigningKey(jwtProperties.getSecretKey()).build()
        .parseClaimsJws(jwt);
    return jws.getBody().getSubject();
  }

}
